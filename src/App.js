import React, { Component } from 'react';
import './App.css';
import Products from './containers/products';
import Cart from './containers/cart';
import Chat from './containers/chat/box';

import Header from './components/navigation/header';
import Footer from './components/navigation/footer';


import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { faCheckSquare, faCoffee, faShoppingCart, faChevronDown, faChevronUp, faComment, faWindowMinimize } from '@fortawesome/free-solid-svg-icons';
 
library.add(fab, faCheckSquare, faCoffee, faShoppingCart, faChevronDown, faChevronUp, faComment, faWindowMinimize);

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <div className="container">
          <Cart/>
          <Products/>
        </div>
        <Chat />
        <Footer/>
      </div>
    );
  }
}

