import * as actionTypes from '../actions/actionTypes';

export const initialState = {
  messages: [],
  responsesSent: 0,
  responses: [
    'Thank you for your message. Someone will get in touch with you soon.',
    'Leave your email and we will contact you.',
    'E-commerce SPA demo. Made by Daumantas Urbanavičius.'
  ]
};

const sendClientMessage = (state, action) => {
  let newMessages = [...state.messages];
  newMessages.push({sender:'client', message: action.payload.message});

  return {...state, messages: newMessages};
};

const sendResponseMessage = (state) => {
  if (state.responsesSent >= state.responses.length) {
    return state;
  }

  let newMessages = [...state.messages];
  newMessages.push({sender:'response', message: state.responses[state.responsesSent]});

  return {...state, messages: newMessages, responsesSent: state.responsesSent + 1};
};

export const reducer = ( state = initialState, action ) => {
  switch ( action.type ) {
  case actionTypes.SEND_CLIENT_MESSAGE: return sendClientMessage(state, action);
  case actionTypes.SEND_RESPONSE_MESSAGE: return sendResponseMessage(state);
  default: return state;
  }
};

export default reducer;