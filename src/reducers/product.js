const initialState = {
  products: [
    {
      id: 1,
      name: 'Milk',
      price: 0.99,
      image: 'img/milk.jpeg',
      description: 'Lorem ipsum dolor sit amet'
    },
    {
      id: 2,
      name: 'Tea',
      price: 1.99,
      image: 'img/tea.jpeg',
      description: 'Lorem ipsum dolor sit amet'
    },
    {
      id: 3,
      name: 'Sugar',
      price: 0.59,
      image: 'img/sugar.jpg',
      description: 'Lorem ipsum dolor sit amet'
    },
    {
      id: 4,
      name: 'Coffee',
      price: 2.89,
      image: 'img/coffee.jpg',
      description: 'Lorem ipsum dolor sit amet'
    },
    {
      id: 5,
      name: 'Butter',
      price: 1.59,
      image: 'img/butter.jpg',
      description: 'Lorem ipsum dolor sit amet'
    },
    {
      id: 6,
      name: 'Apples',
      price: 0.55,
      image: 'img/apple.jpg',
      description: 'Lorem ipsum dolor sit amet'
    },
    {
      id: 7,
      name: 'Bananas',
      price: 0.88,
      image: 'img/banana.jpeg',
      description: 'Lorem ipsum dolor sit amet'
    },
    {
      id: 8,
      name: 'Pears',
      price: 0.79,
      image: 'img/pear.jpeg',
      description: 'Lorem ipsum dolor sit amet'
    },
  ]
};

export const reducer = ( state = initialState ) => {
  return state;
};

export default reducer;