import {reducer, initialState} from './chat';
import * as actionTypes from '../actions/actionTypes';

describe('chat reducer', () => {
  it('should return initial state ', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should send client message', () => {
    expect(reducer(undefined, {payload: {message: 'Hello!'}, type: actionTypes.SEND_CLIENT_MESSAGE})).toEqual({
      ...initialState,
      messages: [{sender: 'client', message:'Hello!'}]
    });
  });

  it('should append another sent message to state', () => {
    expect(reducer({
      ...initialState,
      responsesSent: 1,
      messages: [{sender: 'response', message: initialState.responses[1]}],
    }, {payload: {message: 'Hello!'}, type: actionTypes.SEND_CLIENT_MESSAGE})).toEqual({
      ...initialState,
      messages: [{
        sender: 'response',
        message: initialState.responses[1]
      }, {
        sender: 'client',
        message: 'Hello!'
      }],
      responsesSent: 1
    });
  });

  it('should send second response message and update counter', () => {
    expect(reducer({...initialState, responsesSent: 1}, {type: actionTypes.SEND_RESPONSE_MESSAGE})).toEqual({
      ...initialState,
      messages: [{sender: 'response', message: initialState.responses[1]}],
      responsesSent: 2
    });
  });
});

