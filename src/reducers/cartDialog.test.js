import reducer from './cartDialog';
import * as actionTypes from '../actions/actionTypes';

describe('cartDialog reducer', () => {
  it('should start with a hidden cart dialog', () => {
    expect(reducer(undefined, {})).toEqual({
      show: false
    });
  });

  it('should show the cart dialog when toggling', () => {
    expect(reducer({show: false}, {type: actionTypes.TOGGLE_CART_DIALOG})).toEqual({
      show: true
    });
  });

  it('should hide the cart dialog when toggling', () => {
    expect(reducer({show: true}, {type: actionTypes.TOGGLE_CART_DIALOG})).toEqual({
      show: false
    });
  });
});

