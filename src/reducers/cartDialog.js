import * as actionTypes from '../actions/actionTypes';

const initialState = {
  show: false
};

const toggleCart = (state) => {
  return {
    show: !state.show
  };
};

export const reducer = ( state = initialState, action ) => {
  switch ( action.type ) {
  case actionTypes.TOGGLE_CART_DIALOG: return toggleCart(state);
  default: return state;
  }
};

export default reducer;