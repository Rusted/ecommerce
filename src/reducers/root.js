import { combineReducers } from 'redux';
import cart from './cart';
import product from './product';
import cartDialog from './cartDialog';
import chat from './chat';
export default combineReducers({
  cart,
  product,
  cartDialog,
  chat
});