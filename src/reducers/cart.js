import * as actionTypes from '../actions/actionTypes';

const initialState = {
  basket: [],
  count: 0,
  sum: 0
};

const getCartTotals = (basket) => {
  let count = 0;
  let sum = 0;
  for (var index in basket) {
    count += basket[index].count;
    sum += Math.round(100 * basket[index].price * basket[index].count) / 100;
  }

  return {count, sum: Math.round(100 * sum) / 100};
};

const addProductToCart = (state, action) => {
  const newBasket = [...state.basket];
  const productIndex = newBasket.findIndex(product => product.id === action.payload.product.id);
  if (productIndex === -1) {
    let newProduct = {...action.payload.product};
    newProduct.count = 1;
    newBasket.push(newProduct);
  } else {
    newBasket[productIndex].count++;
  }

  return {
    basket: newBasket,
    ...getCartTotals(newBasket)
  };
};

const removeProductFromCart = (state, action) => {
  const productIndex = state.basket.findIndex(product => product.id === action.payload.product.id);
  if (productIndex === -1 || state.basket[productIndex].count < 1) {
    return state;
  }

  const newBasket = [...state.basket];
  if (newBasket[productIndex].count > 1) {
    newBasket[productIndex].count--;
  } else {
    newBasket.splice(productIndex, 1);
  }

  return {
    basket: newBasket,
    ...getCartTotals(newBasket)
  };
};

export const reducer = ( state = initialState, action ) => {
  switch ( action.type ) {
  case actionTypes.ADD_PRODUCT_TO_CART: return addProductToCart( state, action );
  case actionTypes.REMOVE_PRODUCT_FROM_CART: return removeProductFromCart( state, action );
  default: return state;
  }
};

export default reducer;