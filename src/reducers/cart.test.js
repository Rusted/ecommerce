import reducer from './cart';
import * as actionTypes from '../actions/actionTypes';

describe('cart reducer', () => {

  let productMilk, productBread;

  beforeEach(() => {
    productMilk = {
      id: 1,
      name: 'Milk',
      price: 0.99,
      image: 'img/milk.jpeg'
    };

    productBread = {
      id: 2,
      name: 'Bread',
      price: 2.11,
      image: 'img/bread.png'
    };
  });

  it('should return the initial state - empty basket', () => {
    expect(reducer(undefined, {})).toEqual({
      basket: [],
      count: 0,
      sum: 0
    });
  });

  it('should add product to cart', () => {
    expect(reducer(undefined, {
      type: actionTypes.ADD_PRODUCT_TO_CART,
      payload: {
        product: productMilk
      }
    })).toEqual({
      basket: [{...productMilk, count: 1}],
      count: 1,
      sum: 0.99
    });
  });

  it('should increase cart totals after adding one more product', () => {
    expect(reducer({
      basket: [{...productMilk, count: 1}, {...productBread, count: 1}],
      count: 2,
      sum: 3.10
    }, {
      type: actionTypes.ADD_PRODUCT_TO_CART,
      payload: {
        product: productBread
      }
    })).toEqual({
      basket: [{...productMilk, count: 1}, {...productBread, count: 2}],
      count: 3,
      sum: 5.21
    });
  });

  it('should empty cart after removing the only product', () => {
    expect(reducer({
      basket: [{...productMilk, count: 1}],
      count: 1,
      sum: 0.99
    }, {
      type: actionTypes.REMOVE_PRODUCT_FROM_CART,
      payload: {
        product: productMilk
      }
    })).toEqual({
      basket: [],
      count: 0,
      sum: 0
    });
  });

  it('should have 1 product remaining in the cart after removing different product', () => {
    expect(reducer({
      basket: [{...productMilk, count: 1}, {...productBread, count: 1}],
      count: 2,
      sum: 3.10
    }, {
      type: actionTypes.REMOVE_PRODUCT_FROM_CART,
      payload: {
        product: productBread
      }
    })).toEqual({
      basket: [{...productMilk, count: 1},],
      count: 1,
      sum: 0.99
    });
  });

  it('should decrease product count in the cart after removing product', () => {
    expect(reducer({
      basket: [{...productMilk, count: 1}, {...productBread, count: 2}],
      count: 3,
      sum: 5.21
    }, {
      type: actionTypes.REMOVE_PRODUCT_FROM_CART,
      payload: {
        product: productBread
      }
    })).toEqual({
      basket: [{...productMilk, count: 1}, {...productBread, count: 1}],
      count: 2,
      sum: 3.10
    });
  });
});


