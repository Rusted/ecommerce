import * as actionTypes from '../actions/actionTypes';

export const toggleCartDialog = () => {
  return dispatch => {
    dispatch({
      type: actionTypes.TOGGLE_CART_DIALOG
    });
  };
};