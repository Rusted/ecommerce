import * as actionTypes from '../actions/actionTypes';

export const addToCart = (product) => {
  return dispatch => {
    dispatch({
      type: actionTypes.ADD_PRODUCT_TO_CART,
      payload: {
        product
      }
    });
  };
};

export const removeFromCart = (product) => {
  return dispatch => {
    dispatch({
      type: actionTypes.REMOVE_PRODUCT_FROM_CART,
      payload: {
        product
      }
    });
  };
};