import * as actionTypes from '../actions/actionTypes';

export const sendClientMessage = (message) => dispatch => {
  if (message.length < 1) {
    return;
  }

  dispatch({
    type: actionTypes.SEND_CLIENT_MESSAGE,
    payload: {message}
  });
};
  
export const sendResponseMessage = () => dispatch => {
  dispatch({ type: actionTypes.SEND_RESPONSE_MESSAGE });
};