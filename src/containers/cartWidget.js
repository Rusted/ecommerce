import { connect } from 'react-redux';
import * as actions from '../actions/index';
import React, { Component } from 'react';
import CartWidget from '../components/navigation/cartWidget';

class CartWidgetContainer extends Component {
  render() {
    return (<CartWidget {...this.props}/>);
  }
}

const mapStateToProps = state => {
  return {
    active: state.cartDialog.show,
    sum: state.cart.sum,
    count: state.cart.count
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleCartDialog: () => dispatch(actions.toggleCartDialog()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)( CartWidgetContainer );

