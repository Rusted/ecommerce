import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions/index';
import ChatBox from '../../components/chat/box';

class Chat extends Component {
  
  state = {
    open: false
  }

  toggleChat = () => {
    this.setState({
      open: !this.state.open,
    });
  }

  handleSubmit = (message) => {
    if (message.length < 1) {
      return;
    }
    
    this.props.sendClientMessage(message);
    setTimeout(() => {
      this.props.sendResponseMessage();
    }, 1000);
  }
  
  render() {
    return <ChatBox 
      open={this.state.open} 
      onToggle={this.toggleChat}
      onSubmit={this.handleSubmit}      
      messages={this.props.messages}
    />;
  }
}

const mapStateToProps = state => {
  return {
    messages: state.chat.messages,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    sendClientMessage: (message) => dispatch(actions.sendClientMessage(message)),
    sendResponseMessage: () => dispatch(actions.sendResponseMessage()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)( Chat );