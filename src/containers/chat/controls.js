import React, { Component } from 'react';

class ChatControls extends Component {
  
  state = {
    message: ''
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.onSubmit(this.state.message);
    this.setState({
      message: ''
    });
  }

  setMessage = (event) => {
    this.setState({
      message: event.target.value
    });
  }

  render() {
    return <div className="chat-controls">
      <form onSubmit={this.handleSubmit}>
        <input type="submit" value="Send"/>
        <span><input type="text" name="message" placeholder="Ask your question here..." value={this.state.message} onChange={this.setMessage}/></span>
      </form>
    </div>;
  }
}

export default ChatControls;