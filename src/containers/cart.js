import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/index';
import Dialog from '../components/cart/dialog';

class Cart extends Component {

  render() {
    return <Dialog 
      show={this.props.show}
      basket={this.props.basket} 
      onAdd={this.props.addToCart}
      onRemove={this.props.removeFromCart}
      onClose={this.props.toggleCartDialog}
    />;
  }
}

const mapStateToProps = state => {
  return {
    basket: state.cart.basket,
    show: state.cartDialog.show
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addToCart: (product) => dispatch(actions.addToCart(product)),
    removeFromCart: (product) => dispatch(actions.removeFromCart(product)),
    toggleCartDialog: () => dispatch(actions.toggleCartDialog()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)( Cart );