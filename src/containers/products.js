import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions/index';
import ProductList from '../components/product/list';

class Products extends Component {
  render() {
    return <div className="container">
      <h1>Products</h1>
      <ProductList 
        products={this.props.products}
        basket={this.props.basket}
        onAddToCart={this.props.addToCart}
        onRemoveFromCart={this.props.removeFromCart}
      >
      </ProductList>
    </div>;
  }
}

const mapStateToProps = state => {
  return {
    products: state.product.products,
    basket: state.cart.basket
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addToCart: (product) => dispatch(actions.addToCart(product)),
    removeFromCart: (product) => dispatch(actions.removeFromCart(product)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)( Products );