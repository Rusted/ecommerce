import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const cartWidget = (props) => {
  let className = 'cart-widget';
  if (props.active) {
    className += ' active';
  }
  
  return (
    <div className={className} onClick={props.toggleCartDialog}>
      <div className="widget-text-row">
        <FontAwesomeIcon icon="shopping-cart" />
      </div>
      <div className="widget-text-row">Products: {props.count}</div>
      <div className="widget-text-row">Total: <span className="total-number">{Number.parseFloat(props.sum).toFixed(2)}&nbsp;&euro;</span></div>
      <FontAwesomeIcon icon={props.active ? 'chevron-up' : 'chevron-down'} />
    </div>);
};

export default cartWidget;

