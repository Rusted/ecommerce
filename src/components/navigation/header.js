import React, { Component } from 'react';
import logo from '../../logo.svg';
import CartWidgetContainer from '../../containers/cartWidget';

class Header extends Component {
  render() {
    return (<header className="App-header">
      <div className="container">
        <img src={logo} className="App-logo" alt="logo" />
        <CartWidgetContainer />
      </div>
    </header>);
  }
}

export default Header;