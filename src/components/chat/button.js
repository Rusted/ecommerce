import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const chatBox = () => {
  return <div className="chat-widget">
    <div><FontAwesomeIcon icon="comment" className="chat-icon"/>Start a chat!</div>
  </div>;
};

export default chatBox;