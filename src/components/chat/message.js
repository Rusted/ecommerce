import React from 'react';

const message = (props) => {
  const className = 'chat-message chat-message-' + props.sender;
  const wrapperClassName = 'chat-message-wrapper chat-message-wrapper-' + props.sender;

  return <div className={wrapperClassName}><div className={className}>
    {props.message}
  </div></div>;
};

export default message;