import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ChatBoxMessage from './message';
import ChatControls from '../../containers/chat/controls';

const chatBox = (props) => {
  if (!props.open) {
    return  (<div className="chat-button" onClick={props.onToggle}>
      <FontAwesomeIcon icon="comment" className="chat-icon"/>
    Start a chat!
    </div>);
  }

  return <div className="chat-box">
    <div className="chat-header" onClick={props.onToggle}>
      <FontAwesomeIcon icon="window-minimize" className="minimize-icon"/>
    </div>
    <div className="chat-messages">
      {props.messages.map((message, messageIndex) => (<ChatBoxMessage {...message} key={messageIndex} />))}
    </div>
    <ChatControls onSubmit={props.onSubmit} />
  </div>;
};

export default chatBox;