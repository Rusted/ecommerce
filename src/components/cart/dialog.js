import React from 'react';
import DialogItem from './dialogItem';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const cartDialog = (props) => {
  let className = 'dialog cart-dialog';
  if (props.show === false) {
    className += ' hidden';
  }

  return <div className={className}>
    {props.basket.map((product) => (<DialogItem 
      onAdd={() => props.onAdd(product)}
      onRemove={() => props.onRemove(product)}
      count={product.count}
      sum={product.price * product.count}
      image={product.image}
      key={product.id}
    />))}
    {props.basket.length > 0 
      ? <button type="submit">Checkout</button>
      : <div className="cart-notice">Your shopping cart is empty!</div> 
    }
    <div className="hide-chevron-block" onClick={props.onClose}>
      <FontAwesomeIcon icon="chevron-up"/>
    </div>
  </div>;
};

export default cartDialog;