import React from 'react';

const cartDialog = (props) => {
  return <div className="cart-dialog-item">
    <img src={props.image} alt={props.image} />
    <button onClick={props.onAdd}>+</button>
    <div className="count">{props.count}</div>
    <button onClick={props.onRemove} disabled={props.count < 1}>-</button>
    <div className="sum">{props.sum.toFixed(2)}</div>
  </div>;
};

export default cartDialog;