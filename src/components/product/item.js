import React from 'react';

const ProductItem = (props) => {
  return (<div className="product-card">
    <h4>{props.name}</h4>
    <div className="product-content">
      <div><img className="product-card-image" src={props.image} alt={props.image}/></div>
      <div className="product-price">{props.price}&nbsp;&euro;</div>
      <div className="product-description">{props.description}</div>
      <div className="product-controls">
        <button onClick={props.onAddToCart}>+</button>
        <span>{props.count}</span>
        <button onClick={props.onRemoveFromCart} disabled={props.count < 1}>-</button>
      </div>
    </div>
  </div>);
};

export default ProductItem;