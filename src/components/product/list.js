import React from 'react';
import ProductItem from './item';

const ProductList = (props) => {
  return props.products.map(product => {
    const basketProduct = props.basket.filter(basketItem => {
      return basketItem.id === product.id;
    });
    
    const productBasketCount = basketProduct.length ? basketProduct[0].count : 0;
    return <ProductItem key={product.id}
      {...product}
      count={productBasketCount}
      onAddToCart={() => props.onAddToCart(product)}
      onRemoveFromCart={() => props.onRemoveFromCart(product)}
    >
    </ProductItem>;
  });
};

export default ProductList;