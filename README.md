# Description #
Shopping cart and chat demo - react SPA project
# Installation #
`npm install`
# How to run #
`npm start`
# How to run tests #
`npm start`

`npm test`

Redux unit tests and e2e tests are provided.
# External packages used #
* Puppetteer, babel, faker, jest - for e2e testing
* React-redux - for sharing cart and other data state between components 
* Fontawesome - for small icons, using recommended way
# Author #
Daumantas Urbanavičius
