import faker from 'faker';
import puppeteer from 'puppeteer';

const APP = 'http:/localhost:3000';
let page;
let browser;
const width = 1920;
const height = 1080;

beforeAll(async () => {
  browser = await puppeteer.launch({
    headless: false,
    slowMo: 80,
    args: [`--window-size=${width},${height}`]
  });
  page = await browser.newPage();
  await page.setViewport({ width, height });
});
afterAll(() => {
  browser.close();
});

describe('Open cart dialog', () => {
  test('Empty cart message after clicking on cart', async () => {
    await page.goto(APP);
    await page.waitForSelector('.container');
    await page.click('.cart-widget');
    await page.waitForSelector('.cart-widget.active');
    const emptyMessage = await page.$eval('.cart-notice', e => e.innerHTML);
    expect(emptyMessage).toBe('Your shopping cart is empty!');    
  }, 16000);
});

describe('Add items to cart', () => {
  test('Add different products, check counts, then remove and check for empty cart message', async () => {
    await page.goto(APP);
    await page.waitForSelector('.container');
    await page.click('.cart-widget');
    await page.click('.product-controls button');
    const widgetRows = await page.$$('.widget-text-row');
    const countText = await page.evaluate(el => el.innerHTML, widgetRows[1]);
    const sumText = await page.evaluate(el => el.innerHTML, widgetRows[2]);
    expect(countText).toBe('Products: 1');
    expect(sumText).toBe('Total: <span class="total-number">0.99&nbsp;€</span>');
    const checkoutButton = await page.$$('.cart-dialog button[type="submit"]');
    expect(checkoutButton).toHaveLength(1);
    const cartItems = await page.$$('.cart-dialog .cart-dialog-item');
    expect(cartItems).toHaveLength(1);

    await page.click('.product-card:nth-child(3) button:nth-child(1)');
    await page.click('.product-card:nth-child(3) button:nth-child(1)');
    const widgetRows2 = await page.$$('.widget-text-row');
    const countText2 = await page.evaluate(el => el.innerHTML, widgetRows2[1]);
    const sumText2 = await page.evaluate(el => el.innerHTML, widgetRows2[2]);
    expect(countText2).toBe('Products: 3');
    expect(sumText2).toBe('Total: <span class="total-number">4.97&nbsp;€</span>');
    const cartItems2 = await page.$$('.cart-dialog .cart-dialog-item');
    expect(cartItems2).toHaveLength(2);

    await page.click('.product-card:nth-child(3) button:nth-child(3)');
    await page.click('.product-card:nth-child(3) button:nth-child(3)');
    await page.click('div.cart-dialog-item > button:nth-child(4)');

    const emptyMessage = await page.$eval('.cart-notice', e => e.innerHTML);
    expect(emptyMessage).toBe('Your shopping cart is empty!');  
  }, 16000);
});

describe('Open chat send message', () => {
  test('Open chat send message', async () => {
    await page.goto(APP);
    await page.waitForSelector('.chat-button');
    await page.click('.chat-button');
    await page.waitForSelector('.chat-box');
    await page.click('.chat-box input[type="text"]');
    await page.type('.chat-box input[type="text"]', faker.lorem.sentence());
    await page.click('.chat-box input[type="submit"]');
    await page.waitForSelector('.chat-message-client');
    const messageBlock = await page.$$('.chat-message-client');
    expect(messageBlock).toHaveLength(1);
    await page.waitForSelector('.chat-message-response');
    const messageBlock2 = await page.$$('.chat-message-response');
    expect(messageBlock2).toHaveLength(1);
  }, 16000);
});